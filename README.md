## Phil's Candy Beauty Tela Icon Theme  

### Icon Theme from [Erik Dubois - edu-candy-beauty-tela](https://github.com/erikdubois/edu-candy-beauty-tela)-  
### based on
- [Arcolinux Candy Beauty Icon Theme](https://github.com/arcolinux/arcolinux-candy-beauty.git)  
- [Tela Icon Theme](https://github.com/vinceliuice/Tela-icon-theme)  
- [Candy Icons](https://github.com/EliverLara/candy-icons)  
- [Beautyline](https://gitlab.com/garuda-linux/themes-and-settings/artwork/beautyline)